﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using System;
using UnityEngine.AI;


public class NavMesh : MonoBehaviour
{
    // Start is called before the first frame update
    public Vector3[] mass1; // наш массив координат
   // DirectoryInfo dir = new DirectoryInfo(@"C:\Users\Виктория"); // Путь к папке, где лежат все файлы
    public string FILEName;
    public GameObject prefab;
    Quaternion quaternion;
   // public GameObject PLANE;
    void Start()
    {
        var gameobj = GetComponent<GameObject>();
        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        if (!File.Exists(@FILEName)) { File.Create(@FILEName); } //Проверяем наличие файла
        string[] readText = File.ReadAllLines(@FILEName, Encoding.UTF8);// Читаем данные
        mass1 = new Vector3[readText.Length / 3]; //отмечаем размеры массива
        float[] massd = new float[readText.Length];
        int i = 0;
        foreach (var gr in readText) { massd[i] = float.Parse(gr.Replace('.', ',')); i++; }
        int count = 0;
        for (int j = 0; j < massd.Length; j += 3)
        { mass1[count] = new Vector3(massd[j], massd[j + 1], massd[j + 2]); count++; }
        TrailRenderer trail = GetComponent<TrailRenderer>();
        trail.widthMultiplier = 0;
        quaternion = agent.transform.rotation;
    }
    int i = 0;
    void Update()
    {
        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        TrailRenderer trail = GetComponent<TrailRenderer>();
        
        if (i < mass1.Length)
        {
            agent.destination = mass1[i];
            if ((agent.transform.position.x == mass1[i].x) && (agent.transform.position.z == mass1[i].z))
            {++i; Debug.Log(agent.transform.position);}
        }
        else
        {
            GameObject game = GameObject.Find("Plane");
            agent.destination = game.transform.position;
            if ((agent.transform.position.x == game.transform.position.x) && (agent.transform.position.z == game.transform.position.z))
            {Destroy(gameObject); }
        }
       /* if(agent.transform.rotation!= quaternion)
        {
            //Instantiate(prefab, agent.transform.position, agent.transform.rotation);
            quaternion = agent.transform.rotation;
        }*/
    }
    private void OnMouseDown()
    {
        TrailRenderer trail = GetComponent<TrailRenderer>();
        if(trail.widthMultiplier == 0)
        {trail.widthMultiplier = 1; }
        else { trail.widthMultiplier = 0; }
        
    }

}
