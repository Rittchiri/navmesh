﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RoadCreator))]
public class RoadEditor : Editor
{
    
    RoadCreator creator;
    Vector3 vector;
    Vector3 lastvector;

    void OnSceneGUI()
    {
        if (creator.autoUpdate && Event.current.type == EventType.Repaint)
        {
            creator.UpdateRoad();
            
        }
    }

    void OnEnable()
    {
        creator = (RoadCreator)target;
    }
}
