﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using System;
using UnityEngine.AI;


using PathCreation;

public class MoreMesh : MonoBehaviour
{
    public Transform game, end;
    public GameObject prefab, ets;
    public Vector3[] mass1;
    public string FILEName;
    public List<Vector3> list;
    private Mesh move;
    private Mesh move1,move2;
    bool flag = false;
    Quaternion quaternion;
    private Transform beg;
    GameObject g;
    
    private Vector3 Test,Utest;
    void Start()
    {  
        var gameobj = GetComponent<GameObject>();
        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        if (!File.Exists(@FILEName)) { File.Create(@FILEName); } //Проверяем наличие файла
        string[] readText = File.ReadAllLines(@FILEName, Encoding.UTF8);// Читаем данные
        mass1 = new Vector3[readText.Length / 3]; //отмечаем размеры массива
        float[] massd = new float[readText.Length];
        int i = 0;
        foreach (var gr in readText) { massd[i] = float.Parse(gr.Replace('.', ',')); i++; }
        int count = 0;
        for (int j = 0; j < massd.Length; j += 3)
        { mass1[count] = new Vector3(massd[j], massd[j + 1], massd[j + 2]); count++; }
        quaternion = game.transform.rotation;
        move = prefab.GetComponent<Mesh>();
        move1 = prefab.GetComponent<Mesh>();
        move.point0 = end;
        move.point2 = game;
        move.resolution = 10;
        g = Instantiate(prefab, new Vector3(0, 0.1f, 0), Quaternion.identity) as GameObject;
        g.name = "game" + 0;
        // 

    }
    int i = 0;
    int k = 0;
    int m = 0;
    void Update()
    {
        NavMeshAgent agent = GetComponent<NavMeshAgent>();
      
        if (i < mass1.Length)
        {
            agent.destination = mass1[i];
            list.Add(agent.transform.position);
            if ((agent.transform.position.x == mass1[i].x) && (agent.transform.position.z == mass1[i].z))
            {
              /*  GameObject test = new GameObject();
                test.transform.position = new Vector3(mass1[i].x, 0.1f,mass1[i].z);
                test.transform.rotation = agent.transform.rotation;
                move1.point0 = test.transform;
                move1.point2 = game;
                move1.resolution = 10;
                move2 = GameObject.Find("game" + (k)).GetComponent<Mesh>();
                move2.point2 = move1.point0;
                GameObject r = Instantiate(prefab, new Vector3(0, 0.1f, 0), Quaternion.identity) as GameObject;
                k++;
                r.name = "game" + k;
                quaternion = agent.transform.rotation;
                
                list.Clear();
                m = 0;*/
                ++i;
            }
        }
        else { i = 0; }
    }
}
