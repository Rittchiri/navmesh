﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PathCreator : MonoBehaviour
{
    [HideInInspector]
    public Path path;

    public Color anchorCol = Color.red;
    public Color controlCol = Color.white;
    public Color segmentCol = Color.green;
    public Color selectedSegmentCol = Color.yellow;
    public float anchorDiameter = .1f;
    public float controlDiameter = .075f;
    public bool displayControlPoints = true;
    List<Vector2[]> mass;

    private int numPoints = 50;
    [Range(.05f, 1.5f)]
    public float spacing = 1;
    public float roadWidth = 1;
    public bool autoUpdate;
    public float tiling = 1;
    public GameObject prefab;
    public GameObject game;
    public GameObject rotate;
    
    public void CreatePath()
    {
        path = new Path(transform.position);
    }

    void Reset()
    {
        CreatePath();
    }
    public void Update()
    {
        if((Time.deltaTime*100)%2 ==0)
        {prefab.transform.position = Vector3.zero; }
        else { prefab.transform.position = Vector3.one; }
        
    }

    

}