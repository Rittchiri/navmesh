﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshCube : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject gameObject,target,prefab,pr;
    public Vector3 lefttophight; //= new Vector3(-1, 0, -1);
    public Vector3 righttophight;// = new Vector3(1,0,-1);
    public Vector3 leftbottomhight; //= new Vector3(-1, 0, 1);
    public Vector3 rightbottomhight; //= new Vector3(1, 0, 1);
    List<Vector3> list;
    private int numPoints = 50;
    private Vector3[] positions = new Vector3[50];

    int[] triangles = new int[]
        {
            0,2,3,
            3,1,0,
            4,6,7,
            7,5,4
        };
       

    void Start()
    {

        DrawQuadraticCurve(gameObject.transform.position, prefab.transform.position, target.transform.position);

    }

    // Update is called once per frame
    void Update()
    {
        DrawQuadraticCurve(gameObject.transform.position,prefab.transform.position,target.transform.position);
        for(int i =0; i< positions.Length - 1;i++)
        { MeshFunc(positions[0], positions[i]);Instantiate(pr,positions[i],Quaternion.identity);
          
        }
        
         
      
    }

    void MeshFunc(Vector3 first,Vector3 second)
    {
        MeshFilter filter = GetComponent<MeshFilter>();
        UnityEngine.Mesh mesh = new UnityEngine.Mesh();
        mesh.Clear();
        //
        Vector3 one, two, three, four;
        one.x = first.x + 0.5f;
        one.y = 0;
        one.z = first.z;
        two.x = first.x- 0.5f;
        two.y = 0;
        two.z = first.z;
        three.x = second.x + 0.5f;
        three.y = 0;
        three.z =second.z;
        four.x = second.x - 0.5f;
        four.y = 0;
        four.z = second.z;
        //
        Vector3[] vertices = new Vector3[]
        {
            lefttophight = new Vector3( two.x,two.y,two.z),// new Vector3(-1,0,-1), left top 0
            righttophight = new Vector3(one.x, one.y, one.z),//new Vector3(1,0,-1),// right top 1
            leftbottomhight =  new Vector3(three.x, three.y, three.z),//new Vector3(-1,0,1),//left bottom 2
            rightbottomhight  = new Vector3(four.x, four.y, four.z),// right bottom 3

          
            //back
            leftbottomhight =  new Vector3(three.x, three.y, three.z),//new Vector3(-1,0,1), //right top 4
            rightbottomhight = new Vector3(four.x, four.y, four.z),// new Vector3(1,0,1),// left top 5
            lefttophight = new Vector3( two.x,two.y,two.z),// new Vector3(-1,0,-1),//right bottom 6
            righttophight = new Vector3(one.x, one.y, one.z)//new Vector3(1,0,-1)// left bottom 7
        };
        Debug.Log(lefttophight);
        Debug.Log(righttophight);
        Debug.Log(leftbottomhight);
        Debug.Log(rightbottomhight);

        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        //  mesh.uv = uvs;
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
        mesh.RecalculateBounds();
        filter.mesh = mesh;
    }

    private Vector3 CalculateQuadraticBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        Vector3 p = uu * p0;
        p += 2 * u * t * p1;
        p += tt * p2;
        return p;
    }
    private void DrawQuadraticCurve(Vector3 point0, Vector3 point1, Vector3 point2)
    {

        for (int i = 1; i < numPoints; i++)
        {
            float t = i / (float)numPoints;
            positions[i - 1] = CalculateQuadraticBezierPoint(t, point0, point1, point2);
           

        }
    }
}
