﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using PathCreation;

public class Mesh : MonoBehaviour
{

    public int resolution = 50;
    public Transform point0, point2;
    private int numPoints = 50;
    private Vector3[] positions = new Vector3[50];
    Quaternion quaternion ;
  
    private void Update()
    {
        MakeArcMesh(Draw(point0.transform.position,( point2.transform.position) , point2.transform.position));
    }

    void MakeArcMesh(Vector3[] arcVerts)
    {

        Vector3[] vertices = new Vector3[(resolution + 1) * 2];
        int[] triangles = new int[resolution * 6 * 2];

        for (int i = 0; i <= resolution ; i++)
        {
                
               // vertices[i * 2] = new Vector3(arcVerts[i].x + 0.5f, 0, arcVerts[i].z-0.5f);
              //  vertices[i * 2 + 1] = new Vector3(arcVerts[i].x - 0.5f, 0, arcVerts[i].z + 0.5f);
                 
               if ((point2.transform.rotation.y <= 90) && (point2.transform.rotation.y >= 0))
               {
                vertices[i * 2] = new Vector3(arcVerts[i].x + 0.5f, 0, arcVerts[i].z - 0.5f);
                vertices[i * 2 + 1] = new Vector3(arcVerts[i].x - 0.5f, 0, arcVerts[i].z + 0.5f);
               }

            if ((point2.transform.rotation.y >= -90) && (point2.transform.rotation.y <= 0))
            {
                vertices[i * 2] = new Vector3(arcVerts[i].x + 0.5f, 0, arcVerts[i].z + 0.5f);
                vertices[i * 2 + 1] = new Vector3(arcVerts[i].x - 0.5f, 0, arcVerts[i].z - 0.5f);
            }

           
           

            if (i != resolution)
                {
                    triangles[i * 12] = i * 2;
                    triangles[i * 12 + 1] = triangles[i * 12 + 4] = i * 2 + 1;
                    triangles[i * 12 + 2] = triangles[i * 12 + 3] = (i + 1) * 2;
                    triangles[i * 12 + 5] = (i + 1) * 2 + 1;

                    triangles[i * 12 + 6] = i * 2;
                    triangles[i * 12 + 7] = triangles[i * 12 + 10] = (i + 1) * 2;
                    triangles[i * 12 + 8] = triangles[i * 12 + 9] = i * 2 + 1;
                    triangles[i * 12 + 11] = (i + 1) * 2 + 1;
                }

        }

          UnityEngine.Mesh mesh = new UnityEngine.Mesh();
          MeshFilter meshFilter = GetComponent<MeshFilter>();
          meshFilter.mesh = mesh;
          mesh.vertices = vertices;
          mesh.triangles = triangles;
          mesh.RecalculateNormals();
          mesh.RecalculateBounds();;
    }
   
  


    private Vector3 CalculateQuadraticBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        Vector3 p = uu * p0;
        p += 2 * u * t * p1;
        p += tt * p2;
        return p;
    }
    private void DrawQuadraticCurve(Vector3 point0, Vector3 point1, Vector3 point2)
    {
        
        for (int i = 1; i < numPoints; i++)
        {
            float t = i / (float)numPoints;
            positions[i - 1] = CalculateQuadraticBezierPoint(t, point0, point1, point2);
           
        }
        
        MakeArcMesh(positions);
    }

    Vector3[] Draw(Vector3 point0, Vector3 point1, Vector3 point2)
    {
        Vector3[] arr = new Vector3[resolution + 1];
        for (int i = 1; i <= resolution + 1; i++)
        {
            float t = i / (float)resolution;

            arr[i - 1] = CalculateQuadraticBezierPoint(t, point0, point1, point2);
        }

        return arr;

    }

}
