﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class LaunchAckMesh : MonoBehaviour
{
    public float velocity;
    public float angle;
    public int resolution = 50;
    public float meshWidth;
    UnityEngine.Mesh mesh;
    float g;
    float radianAngle;
    public Vector3[] arr = new Vector3[10];
    public GameObject point0, point1, point2;
    int i = 0;
    private void Awake()
    {
        mesh = GetComponent<MeshFilter>().mesh;
        g = Mathf.Abs(Physics2D.gravity.y);
    }

    private void Start()
    {
       Vector3[] Arr =  Draw(point0.transform.position, point1.transform.position, point2.transform.position);
        for(int i =0;i<Arr.Length; i++)
        { Debug.Log(Arr[i]);

        }
       
    }

    private void Update()
    {
       
       // MakeArcMesh(CalculateArcArray());
        MakeArcMesh(Draw(point0.transform.position, point1.transform.position, point2.transform.position));

    }

    void MakeArcMesh(Vector3[] arcVerts)
    {
        mesh.Clear();
        Vector3[] vertices = new Vector3[(resolution + 1) * 2];
        int[] triangles = new int[resolution * 6 * 2];

        for (int i = 0; i <= resolution ; i++)
        {

            vertices[i * 2] = new Vector3(arcVerts[i].y,0, arcVerts[i].x + meshWidth * 0.5f);
            vertices[i * 2 + 1] = new Vector3(arcVerts[i].y, 0, arcVerts[i].x + meshWidth * -0.5f);
            if (i != resolution)
            {
                triangles[i * 12] = i * 2;
                triangles[i * 12 + 1] = triangles[i * 12 + 4] = i * 2 + 1;
                triangles[i * 12 + 2] = triangles[i * 12 + 3] = (i + 1) * 2;
                triangles[i * 12 + 5] = (i + 1) * 2 + 1;

                triangles[i * 12 + 6] = i * 2;
                triangles[i * 12 + 7] = triangles[i * 12 + 10] = (i + 1) * 2;
                triangles[i * 12 + 8] = triangles[i * 12 + 9] = i * 2 + 1;
                triangles[i * 12 + 11] = (i + 1) * 2 + 1;
            }
           
           
            
        }
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        i = 0;

    }
    Vector3[] CalculateArcArray()
    {
        Vector3[] arcArray = new Vector3[resolution + 1];
        radianAngle = Mathf.Deg2Rad * angle;
        float maxDistance = (velocity * velocity * Mathf.Sin(2 * radianAngle)) / g;
        for (int i = 0; i <= resolution; i++)
        {
            float t = (float)i / (float)resolution;
            arcArray[i] = CalculateArcPoint(t, maxDistance);

        }
        return arcArray;
    }
   
    Vector3 CalculateArcPoint(float t, float maxDistance)
    {
        float x = t * maxDistance;
        float y = x * Mathf.Tan(radianAngle) - ((g * x * x) / (2 * velocity * velocity * Mathf.Cos(radianAngle) * Mathf.Cos(radianAngle)));
        return new Vector3(y, x);
    }

    private Vector3 CalculateQuadraticBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        Vector3 p = uu * p0;
        p += 2 * u * t * p1;
        p += tt * p2;
        return p;
    }

    Vector3[] Draw(Vector3 point0, Vector3 point1, Vector3 point2)
    {
        Vector3[] arr= new Vector3[resolution + 1];
        for (int i = 1; i <= resolution+1; i++)
        {
            float t = i / (float)resolution;

            arr[i - 1] = CalculateQuadraticBezierPoint(t, point0, point1, point2);
        }

        return arr;


    }
}
